/**
 *  Copyright (C) 2011
 *  University of Rochester Department of Computer Science
 *    and
 *  Lehigh University Department of Computer Science and Engineering
 *
 * License: Modified BSD
 *          Please see the file LICENSE.RSTM for licensing information
 */

/**
 * This file includes a BASIC and SIMPLE HashTable, which takes a string as key,
 * and an integer as value.
 *
 */

#ifndef WER_HASHTABLE_HPP
#define WER_HASHTABLE_HPP 1

#include "common.h"
#include <stdio.h>

namespace GTM HIDDEN {

  // entry to save data: <string, integer, next entry>
  struct wer_entry{
      char* st;
      uint32_t data;
      wer_entry *next;
  };

  // bucket to locate data: <integer, first entry>
  struct wer_key_bucket
  {
      uint32_t key;
      wer_entry *first;
  };

  // HastTable
  class wer_hashtable
  {
    private:
      uint32_t num_buckets;     // total num of buckets
      wer_key_bucket *buckets;  // buckets

    public:
      /**
       * constructor
       */
      wer_hashtable() {
          num_buckets = 20;
          buckets = (wer_key_bucket*) calloc(num_buckets, sizeof (wer_key_bucket));
          assert (buckets != NULL);
      }

      /**
       * insert a pair: <string, integer>
       */
      void insert(char* str, uint32_t dat)
      {
          // create a bucket
          wer_entry* new_entry = (wer_entry*) malloc (sizeof (wer_entry));
          new_entry->st = strdup(str);
          new_entry->data = dat;
          new_entry->next = NULL;

          // calculate key
          uint32_t key = strlen(str) % num_buckets;

          // find the right spot and do insert
          if (buckets[key].first == NULL) {
              buckets[key].first = new_entry;
          }
          else {
              wer_entry* it = buckets[key].first;
              while ((it->next) != NULL)
                  it = it->next;
              it->next = new_entry;
          }
      }

      wer_entry* lookup(char* str)
      {
          // calculate key
          uint32_t key = strlen(str) % num_buckets;

          wer_entry* iter = buckets[key].first;
          while (iter != NULL) {
              if (strcmp (str, iter->st) == 0)
                  return iter;
              else
                  iter = iter->next;
          }
          return NULL;
      }

      // specially designed for counting
      bool update (char* str, uint32_t dat)
      {
          wer_entry* entry = lookup(str);
          if (entry == NULL)
              return false;
          entry->data += dat;
          return true;
      }

      void print ()
      {
          for (uint32_t i = 0; i < num_buckets; i++) {
              wer_entry* iter = buckets[i].first;
              if (iter != NULL) {
                  printf("\n");
                  while (iter != NULL) {
                      printf("<%s, %d> -> ", iter->st, iter->data);
                      iter = iter->next;
                  }
              }
          }
      }

      ~wer_hashtable()
      {
          for (uint32_t i = 0; i < num_buckets; i++) {
              wer_entry* iter = buckets[i].first;
              while (iter != NULL) {
                  free (iter);
                  iter = iter->next;
              }
          }
          free (buckets);
      }
  };

} //namespace GTM

#endif // WER_HASHTABLE_HPP

