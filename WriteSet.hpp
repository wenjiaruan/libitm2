/**
 *  Copyright (C) 2011
 *  University of Rochester Department of Computer Science
 *    and
 *  Lehigh University Department of Computer Science and Engineering
 *
 * License: Modified BSD
 *          Please see the file LICENSE.RSTM for licensing information
 */

/**
 *  The RSTM backends that use redo logs all rely on this datastructure,
 *  which provides O(1) clear, insert, and lookup by maintaining a hashed
 *  index into a vector.
 */

#ifndef WRITESET_HPP__
#define WRITESET_HPP__

#include "WriteSetEntry.hpp"

namespace GTM HIDDEN{
  /**
   * We use malloc a couple of times here, and this makes it a bit easier
   */
  template <typename T>
  inline T* typed_malloc(size_t N)
  {
      return static_cast<T*>(malloc(sizeof(T) * N));
  }

  /**
   *  The write set is an indexed array of WriteSetEntry elements.  As with
   *  MiniVector, we make sure that certain expensive but rare functions are
   *  never inlined.
   */
  class WriteSet
  {
#if 0
      // [mfs] TODO: we should make a variant of the WriteSet that uses this
      //       instead of the magic constant "8"
      static const int SPILL_FACTOR = 8; // 8 probes and we resize the list...
#endif

      /***  data type for the index */
      struct index_t
      {
          size_t version;
          void*  address;
          size_t index;

          index_t() : version(0), address(NULL), index(0) { }
      };

      index_t* index;                             // hash entries
      size_t   shift;                             // for the hash function
      size_t   ilength;                           // max size of hash
      size_t   version;                           // version for fast clearing

      WriteSetEntry* list;                        // the array of actual data
      size_t   capacity;                          // max array size
      size_t   lsize;                             // elements in the array

      /**
       *  hash function is straight from CLRS (that's where the magic
       *  constant comes from).
       */
      size_t hash(void* const key) const
      {
          static const unsigned long long s = 2654435769ull;
          const unsigned long long r = ((unsigned long long)key) * s;
          return (size_t)((r & 0xFFFFFFFF) >> shift);
      }

      /**
       *  This doubles the size of the index. This *does not* do anything as
       *  far as actually doing memory allocation. Callers should delete[]
       *  the index table, increment the table size, and then reallocate it.
       */
      inline size_t doubleIndexLength()
      {
          assert(shift != 0 &&
                 "ERROR: the writeset doesn't support an index this large");
          shift   -= 1;
          ilength  = 1 << (8 * sizeof(uint32_t) - shift);
          return ilength;
      }


      /**
       *  Supporting functions for resizing.  Note that these are never
       *  inlined.
       */
      /***  Rebuild the writeset */
      void rebuild()
      {
          assert(version != 0 && "ERROR: the version should *never* be 0");

          // extend the index
          //delete[] index;
          free (index);

          //index = new index_t[doubleIndexLength()];
          // [wer] use calloc to assign 0, so that we don't need constructor
          index = (index_t*)calloc (doubleIndexLength(), sizeof(index_t));

          for (size_t i = 0; i < lsize; ++i) {
              const WriteSetEntry& l = list[i];
              size_t h = hash(l.addr.vp);

              // search for the next available slot
              while (index[h].version == version)
                  h = (h + 1) % ilength;

              index[h].address = l.addr.vp;
              index[h].version = version;
              index[h].index   = i;
          }
      }

      /***  Resize the writeset */
      void resize()
      {
          WriteSetEntry* temp  = list;
          capacity     *= 2;
          list          = typed_malloc<WriteSetEntry>(capacity);
          memcpy(list, temp, sizeof(WriteSetEntry) * lsize);
          free(temp);
      }

      /***  Another writeset reset function that we don't want inlined */
      void reset_internal()
      {
          memset(index, 0, sizeof(index_t) * ilength);
          version = 1;
      }


    public:
      //WriteSet(const size_t initial_capacity);
      WriteSet() {
          index = NULL;
          shift = 8 * sizeof (uint32_t);
          ilength = 0;
          version = 1;
          list = NULL;
          capacity = 64; // [wer] HARD code 64 as in rstm
          lsize = 0;

          // Find a good index length for the initial capacity of the list.
          while (ilength < 3 * capacity)
              doubleIndexLength();

          //index = new index_t[ilength];
          index = (index_t*)calloc (ilength, sizeof(index_t));
          list  = typed_malloc<WriteSetEntry>(capacity);
      }


      ~WriteSet() {
          //delete[] index;
          free(index);
          free(list);
      }

      /**
       *  Search function.  The log is an in/out parameter, and the bool
       *  tells if the search succeeded. When we are byte-logging, the log's
       *  mask is updated to reflect the bytes in the returned value that are
       *  valid. In the case that we don't find anything, the mask is set to 0.
       */
      bool find(WriteSetEntry& log) const
      {
          size_t h = hash(log.addr.vp);

          while (index[h].version == version) {
              if (index[h].address != log.addr.vp) {
                  // continue probing
                  h = (h + 1) % ilength;
                  continue;
              }
#if defined(STM_WS_WORDLOG)
              log.val = list[index[h].index].val;
              return true;
#elif defined(STM_WS_BYTELOG)
              // Need to intersect the mask to see if we really have a match. We
              // may have a full intersection, in which case we can return the
              // logged value. We can have no intersection, in which case we can
              // return false. We can also have an awkward intersection, where
              // we've written part of what we're trying to read. In that case,
              // the "correct" thing to do is to read the word from memory, log
              // it, and merge the returned value with the partially logged
              // bytes.
              WriteSetEntry& entry = list[index[h].index];
              if (__builtin_expect((log.mask & entry.mask) == 0, false)) {
                  log.mask = 0;
                  return false;
              }

              // The update to the mask transmits the information the caller
              // needs to know in order to distinguish between a complete and a
              // partial intersection.
              log.val = entry.val;
              log.mask = entry.mask;
              return true;
#else
#error "Preprocessor configuration error."
#endif
          }

#if defined(STM_WS_BYTELOG)
          log.mask = 0x0; // report that there were no intersecting bytes
#endif
          return false;
      }

      // only used in final_validate()
      bool find_orec(WriteSetEntry& log) const
      {
          size_t h = hash(log.addr.vp);

          while (index[h].version == version) {
              if (index[h].address != log.addr.vp) {
                  // continue probing
                  h = (h + 1) % ilength;
                  continue;
              }
#if defined(STM_WS_WORDLOG)
              log.val = list[index[h].index].val;
              log.which = list[index[h].index].which;
              for (int i = 0; i < 8; i++)
                  log.orec_val[i] = list[index[h].index].orec_val[i];
              return true;
#elif defined(STM_WS_BYTELOG)
              // Need to intersect the mask to see if we really have a match. We
              // may have a full intersection, in which case we can return the
              // logged value. We can have no intersection, in which case we can
              // return false. We can also have an awkward intersection, where
              // we've written part of what we're trying to read. In that case,
              // the "correct" thing to do is to read the word from memory, log
              // it, and merge the returned value with the partially logged
              // bytes.
              WriteSetEntry& entry = list[index[h].index];
              if (__builtin_expect((log.mask & entry.mask) == 0, false)) {
                  log.mask = 0;
                  return false;
              }

              // The update to the mask transmits the information the caller
              // needs to know in order to distinguish between a complete and a
              // partial intersection.
              log.val = entry.val;
              log.mask = entry.mask;
              return true;
#else
#error "Preprocessor configuration error."
#endif
          }

#if defined(STM_WS_BYTELOG)
          log.mask = 0x0; // report that there were no intersecting bytes
#endif
          return false;
      }

      // same as find, but take addr as parameter
      bool find_addr(void* log) const
      {
          size_t h = hash(log);

          while (index[h].version == version) {
              if (index[h].address != log) {
                  // continue probing
                  h = (h + 1) % ilength;
                  continue;
              }
#if defined(STM_WS_WORDLOG)
              return true;
#elif defined(STM_WS_BYTELOG)
              assert (false && "doesn't support bytelog.");
#else
#error "Preprocessor configuration error."
#endif
          }
          return false;
      }

      // [wer] this function is basicaly same as find(), but only used for opslog(),
      //       as we need find() function to return the pointer to the element
      WriteSetEntry* getelement(WriteSetEntry& log) const
      {
          size_t h = hash(log.addr.vp);

          while (index[h].version == version) {
              if (index[h].address != log.addr.vp) {
                  // continue probing
                  h = (h + 1) % ilength;
                  continue;
              }
#if defined(STM_WS_WORDLOG)
              log.val = list[index[h].index].val;
              //return true;
              return &list[index[h].index];
#elif defined(STM_WS_BYTELOG)
              // Need to intersect the mask to see if we really have a match. We
              // may have a full intersection, in which case we can return the
              // logged value. We can have no intersection, in which case we can
              // return false. We can also have an awkward intersection, where
              // we've written part of what we're trying to read. In that case,
              // the "correct" thing to do is to read the word from memory, log
              // it, and merge the returned value with the partially logged
              // bytes.
              WriteSetEntry& entry = list[index[h].index];
              if (__builtin_expect((log.mask & entry.mask) == 0, false)) {
                  log.mask = 0;
                  return NULL;
              }

              // The update to the mask transmits the information the caller
              // needs to know in order to distinguish between a complete and a
              // partial intersection.
              log.val = entry.val;
              log.mask = entry.mask;
              //return true;
              return &list[index[h].index];
#else
#error "Preprocessor configuration error."
#endif
          }

#if defined(STM_WS_BYTELOG)
          log.mask = 0x0; // report that there were no intersecting bytes
#endif
          return NULL;
      }

      //[wer210] find and remove an element: find the index, and mark version -1 to make
      // this index available again, o(1) time.
      //[CAUTION] this should only be used for ops, as the WriteSetEntry is not
      // actually removed from the list, but marked its val = 0.
      bool remove(WriteSetEntry& log) const
      {
          size_t h = hash(log.addr.vp);

          while (index[h].version == version) {
              if (index[h].address != log.addr.vp) {
                  // continue probing
                  h = (h + 1) % ilength;
                  continue;
              }
#if defined(STM_WS_WORDLOG)
              // save the value into log
              log.val = list[index[h].index].val;
              // mark value of the element 0
              // mark index version -1 so that this index is available again
              list[index[h].index].val.vp = 0;
              index[h].version = -1;
              return  true;
#elif defined(STM_WS_BYTELOG)
              // Need to intersect the mask to see if we really have a match. We
              // may have a full intersection, in which case we can return the
              // logged value. We can have no intersection, in which case we can
              // return false. We can also have an awkward intersection, where
              // we've written part of what we're trying to read. In that case,
              // the "correct" thing to do is to read the word from memory, log
              // it, and merge the returned value with the partially logged
              // bytes.
              WriteSetEntry& entry = list[index[h].index];
              if (__builtin_expect((log.mask & entry.mask) == 0, false)) {
                  log.mask = 0;
                  return false;
              }

              // The update to the mask transmits the information the caller
              // needs to know in order to distinguish between a complete and a
              // partial intersection.
              log.val = entry.val;
              log.mask = entry.mask;

              // mark value of the element 0
              // mark index version -1 so that this index is available again
              list[index[h].index].val.vp = 0;
              index[h].version = -1;
              return true;
#else
#error "Preprocessor configuration error."
#endif
          }

#if defined(STM_WS_BYTELOG)
          log.mask = 0x0; // report that there were no intersecting bytes
#endif
          return false;
      }


      //[wer] not implemented yet.
#if 0
      /**
       *  Support for abort-on-throw rollback tricky.  We might need to write
       *  to an exception object.
       *
       *  NB: We use a macro to hide the fact that some rollback calls are
       *      really simple.  This gets called by ~30 STM implementations
       */
#if !defined (STM_ABORT_ON_THROW)
      void rollback() { }
#   define STM_ROLLBACK(log, exception, len) log.rollback()
#else
      void rollback(void**, size_t);
#   define STM_ROLLBACK(log, exception, len) log.rollback(exception, len)
#endif
#endif

      /**
       *  Encapsulate writeback in this routine, so that we can avoid making
       *  modifications to lots of STMs when we need to change writeback for a
       *  particular compiler.
       */
      inline void writeback()
      {
          for (iterator i = begin(), e = end(); i != e; ++i)
              i->writeback();
      }

      // [wer] for now we use WriteSet to save addr and value as a read value set
      // for NOrec like algorithm, this function is used to validate each entry
      inline bool valuecheck()
      {
          bool valid = true;
          for (iterator i = begin(), e = end(); i != e; ++i)
              valid &= (i->checkvalue());
          return valid;
      }

      // [wer] for ops()
      inline void operate()
      {
          for (iterator i = begin(), e = end(); i != e; ++i)
              i->operate();
      }

      /**
       *  Inserts an entry in the write set.  Coalesces writes, which can
       *  appear as write reordering in a data-racy program.
       */
      void insert(const WriteSetEntry& log)
      {
          size_t h = hash(log.addr.vp);

          //  Find the slot that this address should hash to. If we find it,
          //  update the value. If we find an unused slot then it's a new
          //  insertion.
          while (index[h].version == version) {
              if (index[h].address != log.addr.vp) {
                  h = (h + 1) % ilength;
                  continue; // continue probing at new h
              }

              // there /is/ an existing entry for this word, we'll be updating
              // it no matter what at this point
              list[index[h].index].update(log);
              return;
          }

          // add the log to the list (guaranteed to have space)
          list[lsize] = log;

          // update the index
          index[h].address = log.addr.vp;
          index[h].version = version;
          index[h].index   = lsize;

          // update the end of the list
          lsize += 1;

          // resize the list if needed
          if (__builtin_expect(lsize == capacity, false))
              resize();

          // if we reach our load-factor
          // NB: load factor could be better handled rather than the magic
          //     constant 3 (used in constructor too).
          if (__builtin_expect((lsize * 3) >= ilength, false))
              rebuild();
#if 0
          // [mfs] I think we might want to consider resizing more frequently...
          if (__builtin_expect(spills > SPILL_FACTOR, false)) {
              printf("spillflow\n");
              rebuild();
          }
#endif
      }

      /**
       *  Inserts an entry in the OPS-write set.
       *  commutative operations
       */
      void commu_insert(const WriteSetEntry& log)
      {
          size_t h = hash(log.addr.vp);

          //  Find the slot that this address should hash to. If we find it,
          //  update the value. If we find an unused slot then it's a new
          //  insertion.
          while (index[h].version == version) {
              if (index[h].address != log.addr.vp) {
                  h = (h + 1) % ilength;
                  continue; // continue probing at new h
              }

              // there /is/ an existing entry for this word, we'll be updating
              // it no matter what at this point
              //[wer210] the only difference from insert()
              list[index[h].index].commu_update(log);
              return;
          }

          // add the log to the list (guaranteed to have space)
          list[lsize] = log;

          // update the index
          index[h].address = log.addr.vp;
          index[h].version = version;
          index[h].index   = lsize;

          // update the end of the list
          lsize += 1;

          // resize the list if needed
          if (__builtin_expect(lsize == capacity, false))
              resize();

          // if we reach our load-factor
          // NB: load factor could be better handled rather than the magic
          //     constant 3 (used in constructor too).
          if (__builtin_expect((lsize * 3) >= ilength, false))
              rebuild();
      }

      /**
       *  Inserts an entry in the OPS-write set.
       *  commutative operations
       *  [NOTE] Including updating orecs
       */
      void commu_insert_including_orecs(const WriteSetEntry& log)
      {
          size_t h = hash(log.addr.vp);

          //  Find the slot that this address should hash to. If we find it,
          //  update the value. If we find an unused slot then it's a new
          //  insertion.
          while (index[h].version == version) {
              if (index[h].address != log.addr.vp) {
                  h = (h + 1) % ilength;
                  continue; // continue probing at new h
              }

              // there /is/ an existing entry for this word, we'll be updating
              // it no matter what at this point
              //[wer210] the only difference from commu_insert()
              list[index[h].index].commu_update(log);
              list[index[h].index].update_orecs(log);
              return;
          }

          // add the log to the list (guaranteed to have space)
          list[lsize] = log;

          // update the index
          index[h].address = log.addr.vp;
          index[h].version = version;
          index[h].index   = lsize;

          // update the end of the list
          lsize += 1;

          // resize the list if needed
          if (__builtin_expect(lsize == capacity, false))
              resize();

          // if we reach our load-factor
          // NB: load factor could be better handled rather than the magic
          //     constant 3 (used in constructor too).
          if (__builtin_expect((lsize * 3) >= ilength, false))
              rebuild();
      }


      /*** size() lets us know if the transaction is read-only */
      size_t size() const { return lsize; }

      /*** will_reorg() lets us know if an insertion will cause a reorg of the data structure */
      bool will_reorg() const {
        size_t nsize = lsize + 1;
        return ((nsize == capacity) || ((nsize * 3) >= ilength));
      }

      /**
       *  We use the version number to reset in O(1) time in the common case
       */
      void reset()
      {
          lsize    = 0;
          version += 1;

          // check overflow
          if (version != 0)
              return;
          reset_internal();
      }

      /*** Iterator interface: iterate over the list, not the index */
      typedef WriteSetEntry* iterator;
      iterator begin() const { return list; }
      iterator end()   const { return list + lsize; }
  };// WriteSet
}//namespace GTM

#endif // WRITESET_HPP__
