/**
 *  Copyright (C) 2011
 *  University of Rochester Department of Computer Science
 *    and
 *  Lehigh University Department of Computer Science and Engineering
 *
 * License: Modified BSD
 *          Please see the file LICENSE.RSTM for licensing information
 */

/**
 *  The RSTM backends that use redo logs all rely on this datastructure,
 *  which provides O(1) clear, insert, and lookup by maintaining a hashed
 *  index into a vector.
 */

#ifndef WRITESETENTRY_HPP__
#define WRITESETENTRY_HPP__

#include <cassert>
#include <stdint.h>
#include "common.h"

#define OP_P 1  // plus
#define OP_S -1 // substract
#define OP_M 2  // multiply
#define OP_D 3  // divide
#define OP_DELETED 4  // an deleted entry

namespace GTM HIDDEN
{
  typedef union
  {
      unsigned char           c;
      short unsigned int      s;
      unsigned int            i;
      long unsigned int       l;
      float                   f;
      double                  d;
      long double             ld;
      __complex__ float       cf;
      __complex__ double      cd;
      __complex__ long double cld;
      unsigned long long      ull;
      void*                   vp;
  } val_t;

  typedef union
  {
      unsigned char*           c;
      short unsigned int*      s;
      unsigned int*            i;
      long unsigned int*       l;
      float*                   f;
      double*                  d;
      long double*             ld;
      __complex__ float*       cf;
      __complex__ double*      cd;
      __complex__ long double* cld;
      unsigned long long*      ull;
      void*                    vp;         // this one is a convenience...
  } addr_t;
  typedef unsigned int gtm_word __attribute__((mode (word)));
  /**
   *  The WriteSet implementation is heavily influenced by the configuration
   *  parameters, STM_WS_(WORD/BYTE)LOG, and STM_ABORT_ON_THROW. This means
   *  that much of this file is ifdeffed accordingly.
   */

  /**
   * The log entry type when we're word-logging is pretty trivial, and just
   * logs address/value pairs.
   */
  struct WordLoggingWriteSetEntry
  {
      //void** addr;
      //void*  val;
      addr_t addr;
      val_t  val;
      int    which;
      int    op;                      // Operation type, "-1" for -=, "1" for +=
      gtm_word orec_val[8];           // Save old orec value of this location here
                                      //      the size of a location is at most 8-byte
                                      //      so we at most have 8 orecs in one entry
      std::atomic<gtm_word> *orecs[8];              // Pointers to the orecs
      int orec_len;
      WordLoggingWriteSetEntry()
      { addr.c = NULL; val.c = 0; which = 0; op = OP_P; orec_len = 0;
          for (int i = 0; i < 8; i++) {
              orec_val[i] = 0;
              orecs[i] = NULL;
          }
      }

      /**
       *  Called when we are WAW an address, and we want to coalesce the
       *  write. Trivial for the word-based writeset, but complicated for the
       *  byte-based version.
       */
      void update(const WordLoggingWriteSetEntry& rhs) { val = rhs.val; }
      void commu_update(const WordLoggingWriteSetEntry& rhs) { val.l += rhs.val.l; }
      void update_orecs(const WordLoggingWriteSetEntry& rhs) {
          for (uint32_t i = 0; i < 8; i++) {
              orec_val[i] = rhs.orec_val[i];
              orecs[i] = rhs.orecs[i];
          }
      }

      /**
       * Check to see if the entry is completely contained within the given
       * address range. We have some preconditions here w.r.t. alignment and
       * size of the range. It has to be at least word aligned and word
       * sized. This is currently only used with stack addresses, so we don't
       * include asserts because we don't want to pay for them in the common
       * case writeback loop.
       */
      bool filter(void* lower, void* upper)
      {
          return !((long long)addr.vp + 1 < (long long)lower ||
                   (long long)addr.vp >= (long long )upper);
      }

      /**
       * Called during writeback to actually perform the logged write. This is
       * trivial for the word-based set, but the byte-based set is more
       * complicated.
       */
      //void writeback() const { *addr = val; }

      /*** do a writeback.  This should incorporate a mask eventually ***/
      void writeback() const
      {
          switch (which) {
            case 0: *addr.c = val.c;      break;
            case 1: *addr.s = val.s;      break;
            case 2: *addr.i = val.i;      break;
            case 3: *addr.l = val.l;      break;
            case 4: *addr.f = val.f;      break;
            case 5: *addr.d = val.d;      break;
            case 6: *addr.ld = val.ld;    break;
            case 7: *addr.cf = val.cf;    break;
            case 8: *addr.cd = val.cd;    break;
            case 9: *addr.cld = val.cld;  break;
            case 10: *addr.ull = val.ull; break;
            default:
              assert(0 && "Invalid writeback type");
          };
      }

      /*** do certain operation to the address, for now just write *addr+val back to *addr ***/
#if 0
      void operate() const
      {
          switch (which) {
            case 0: *addr.c += val.c;      break;
            case 1: *addr.s += val.s;      break;
            case 2: *addr.i += val.i;      break;
            case 3: *addr.l += val.l;      break;
            case 4: *addr.f += val.f;      break;
            case 5: *addr.d += val.d;      break;
            case 6: *addr.ld += val.ld;    break;
            case 7: *addr.cf += val.cf;    break;
            case 8: *addr.cd += val.cd;    break;
            case 9: *addr.cld += val.cld;  break;
            case 10: *addr.ull += val.ull; break;
            default:
              assert(0 && "Invalid operate type");
          };
      }
#endif
      void operate() const
      {
          switch (which) {
            case 0:
              switch (op) {
                case OP_P: *addr.c += val.c;      break;
                case OP_S: *addr.c -= val.c;      break;
                case OP_M: *addr.c *= val.c;      break;
                case OP_D: *addr.c /= val.c;      break;
              }; break;
            case 1:
              switch (op) {
                case OP_P: *addr.s += val.s;      break;
                case OP_S: *addr.s -= val.s;      break;
                case OP_M: *addr.s *= val.s;      break;
                case OP_D: *addr.s /= val.s;      break;
              }; break;
            case 2:
              switch (op) {
                case OP_P: *addr.i += val.i;      break;
                case OP_S: *addr.i -= val.i;      break;
                case OP_M: *addr.i *= val.i;      break;
                case OP_D: *addr.i /= val.i;      break;
              }; break;
            case 3:
              switch (op) {
                case OP_P: *addr.l += val.l;      break;
                case OP_S: *addr.l -= val.l;      break;
                case OP_M: *addr.l *= val.l;      break;
                case OP_D: *addr.l /= val.l;      break;
              }; break;
            case 4:
              switch (op) {
                case OP_P: *addr.f += val.f;      break;
                case OP_S: *addr.f -= val.f;      break;
                case OP_M: *addr.f *= val.f;      break;
                case OP_D: *addr.f /= val.f;      break;
              }; break;
            case 5:
              switch (op) {
                case OP_P: *addr.d += val.d;      break;
                case OP_S: *addr.d -= val.d;      break;
                case OP_M: *addr.d *= val.d;      break;
                case OP_D: *addr.d /= val.d;      break;
              }; break;
            case 6:
              switch (op) {
                case OP_P: *addr.ld += val.ld;      break;
                case OP_S: *addr.ld -= val.ld;      break;
                case OP_M: *addr.ld *= val.ld;      break;
                case OP_D: *addr.ld /= val.ld;      break;
              }; break;
            case 7:
              switch (op) {
                case OP_P: *addr.cf += val.cf;      break;
                case OP_S: *addr.cf -= val.cf;      break;
                case OP_M: *addr.cf *= val.cf;      break;
                case OP_D: *addr.cf /= val.cf;      break;
              }; break;
            case 8:
              switch (op) {
                case OP_P: *addr.cd += val.cd;      break;
                case OP_S: *addr.cd -= val.cd;      break;
                case OP_M: *addr.cd *= val.cd;      break;
                case OP_D: *addr.cd /= val.cd;      break;
              }; break;
            case 9:
              switch (op) {
                case OP_P: *addr.cld += val.cld;      break;
                case OP_S: *addr.cld -= val.cld;      break;
                case OP_M: *addr.cld *= val.cld;      break;
                case OP_D: *addr.cld /= val.cld;      break;
              }; break;
            case 10:
              switch (op) {
                case OP_P: *addr.ull += val.ull;      break;
                case OP_S: *addr.ull -= val.ull;      break;
                case OP_M: *addr.ull *= val.ull;      break;
                case OP_D: *addr.ull /= val.ull;      break;
              }; break;
            default:
              assert(0 && "Invalid operate type");
          };
      }

      // [wer] used for each entry in NOrec value list to validate
      bool checkvalue()
      {
          switch (which) {
            case 0: return (*addr.c == val.c);
            case 1: return (*addr.s == val.s);
            case 2: return (*addr.i == val.i);
            case 3: return (*addr.l == val.l);
            case 4: return (*addr.f == val.f);
            case 5: return (*addr.d == val.d);
            case 6: return (*addr.ld == val.ld);
            case 7: return (*addr.cf == val.cf);
            case 8: return (*addr.cd == val.cd);
            case 9: return (*addr.cld == val.cld);
            case 10: return (*addr.ull == val.ull);
            default:
              assert(0 && "Invalid checkvalue type");
          };
      }

      size_t size()
      {
          switch (which)
          {
            case  0: return sizeof(unsigned char);
            case  1: return sizeof(short unsigned int);
            case  2: return sizeof(unsigned int);
            case  3: return sizeof(long unsigned int);
            case  4: return sizeof(float);
            case  5: return sizeof(double);
            case  6: return sizeof(long double);
            case  7: return sizeof(__complex__ float);
            case  8: return sizeof(__complex__ double);
            case  9: return sizeof(__complex__ long double);
            case 10: return sizeof(unsigned long long);
          }
          return 0;
      }

      /**
       * Called during rollback if there is an exception object that we need to
       * perform writes to. The address range is the range of addresses that
       * we're looking for. If this log entry is contained in the range, we
       * perform the writeback.
       *
       * NB: We're assuming a pretty well defined address range, in terms of
       *     size and alignment here, because the word-based writeset can only
       *     handle word-sized data.
       */
      void rollback(void** lower, void** upper)
      {
          assert(false && "[wer] NOT implemented yet!.");
          /*
          assert((uint8_t*)upper - (uint8_t*)lower >= (int)sizeof(void*));
          assert((uintptr_t)upper % sizeof(void*) == 0);
          if (addr >= lower && (addr + 1) <= upper)
          writeback();
          */
      }
  };

  //////////////////////////////////////////////////
  // our WordLoggingWriteSetEntry object is a rather simple struct of unions... the problem
  // is that we need to allow templated read/write functions to access the
  // correct fields of the unions.  To help us out in that effort, we provide
  // two templated functions: the first is for reading the "right" data out
  // of the union, the second is for writing data into the union the "right"
  // way.

  // These functions are for reading from the union
  //
  // NB: the second parameter is a dummy parameter, but we need it because
  // C++ doesn't disambiguate templated functions based solely on return
  // value
  template <typename T>
  T wslog_extract_data(WordLoggingWriteSetEntry e, const T* addr);

  template <typename T = unsigned char>
  unsigned char wslog_extract_data(WordLoggingWriteSetEntry e, const unsigned char* addr)
  { return e.val.c; }

  template <typename T = short unsigned int>
  short unsigned int wslog_extract_data(WordLoggingWriteSetEntry e, const short unsigned int* addr)
  { return e.val.s; }

  template <typename T = unsigned int>
  unsigned int wslog_extract_data(WordLoggingWriteSetEntry e, const unsigned int* addr)
  { return e.val.i; }

  template <typename T = long unsigned int>
  long unsigned int wslog_extract_data(WordLoggingWriteSetEntry e, const long unsigned int* addr)
  { return e.val.l; }

  template <typename T = float>
  float wslog_extract_data(WordLoggingWriteSetEntry e, const float* addr)
  { return e.val.f; }

  template <typename T = double>
  double wslog_extract_data(WordLoggingWriteSetEntry e, const double* addr)
  { return e.val.d; }

  template <typename T = long double>
  long double wslog_extract_data(WordLoggingWriteSetEntry e, const long double* addr)
  { return e.val.ld; }

  template <typename T = __complex__ float>
  __complex__ float wslog_extract_data(WordLoggingWriteSetEntry e, const __complex__ float* addr)
  { return e.val.cf; }

  template <typename T = __complex__ double>
  __complex__ double wslog_extract_data(WordLoggingWriteSetEntry e, const __complex__ double* addr)
  { return e.val.cd; }

  template <typename T = __complex__ long double>
  __complex__ long double wslog_extract_data(WordLoggingWriteSetEntry e, const __complex__ long double* addr)
  { return e.val.cld; }

  template <typename T = unsigned long long>
  unsigned long long wslog_extract_data(WordLoggingWriteSetEntry e, const unsigned long long* addr)
  { return e.val.ull; }



  template <typename T>
  void wslog_insert_data(WordLoggingWriteSetEntry* e, T* addr, T val);

  template <typename T = unsigned char>
  void wslog_insert_data(WordLoggingWriteSetEntry* e, unsigned char* addr, unsigned char val)
  { e->addr.c = addr;      e->val.c = val;     e->which = 0; }

  template <typename T = short unsigned int>
  void wslog_insert_data(WordLoggingWriteSetEntry* e, short unsigned int* addr, short unsigned int val)
  { e->addr.s = addr;      e->val.s = val;     e->which = 1; }

  template <typename T = unsigned int>
  void wslog_insert_data(WordLoggingWriteSetEntry* e, unsigned int* addr, unsigned int val)
  { e->addr.i = addr;      e->val.i = val;     e->which = 2; }

  template <typename T = long unsigned int>
  void wslog_insert_data(WordLoggingWriteSetEntry* e, long unsigned int* addr, long unsigned int val)
  { e->addr.l = addr;      e->val.l = val;     e->which = 3; }

  template <typename T = float>
  void wslog_insert_data(WordLoggingWriteSetEntry* e, float* addr, float val)
  { e->addr.f = addr;      e->val.f = val;     e->which = 4; }

  template <typename T = double>
  void wslog_insert_data(WordLoggingWriteSetEntry* e, double* addr, double val)
  { e->addr.d = addr;      e->val.d = val;     e->which = 5; }

  template <typename T = long double>
  void wslog_insert_data(WordLoggingWriteSetEntry* e, long double* addr, long double val)
  { e->addr.ld = addr;     e->val.ld = val;    e->which = 6; }

  template <typename T = __complex__ float>
  void wslog_insert_data(WordLoggingWriteSetEntry* e, __complex__ float* addr, __complex__ float val)
  { e->addr.cf = addr;     e->val.cf = val;    e->which = 7; }

  template <typename T = __complex__ double>
  void wslog_insert_data(WordLoggingWriteSetEntry* e, __complex__ double* addr, __complex__ double val)
  { e->addr.cd = addr;     e->val.cd = val;    e->which = 8; }

  template <typename T = __complex__ long double>
  void wslog_insert_data(WordLoggingWriteSetEntry* e, __complex__ long double* addr, __complex__ long double val)
  { e->addr.cld = addr;    e->val.cld = val;   e->which = 9; }

  template <typename T = unsigned long long>
  void wslog_insert_data(WordLoggingWriteSetEntry* e, unsigned long long* addr, unsigned long long val)
  { e->addr.ull = addr;    e->val.ull = val;   e->which = 10; }


  template <typename T>
  void single_operate_on(WordLoggingWriteSetEntry *e,  T* addr);

  template <typename T = unsigned char>
  void  single_operate_on(WordLoggingWriteSetEntry *e,  unsigned char* addr)
  {
      switch (e->op) {
        case OP_P: *addr += e->val.c;      break;
        case OP_S: *addr -= e->val.c;      break;
        case OP_M: *addr *= e->val.c;      break;
        case OP_D: *addr /= e->val.c;
      };
  }

  template <typename T = short unsigned int>
  void single_operate_on(WordLoggingWriteSetEntry *e,  short unsigned int* addr)
  {
      switch (e->op) {
        case OP_P: *addr += e->val.s;      break;
        case OP_S: *addr -= e->val.s;      break;
        case OP_M: *addr *= e->val.s;      break;
        case OP_D: *addr /= e->val.s;
      };
  }

  template <typename T = unsigned int>
  void single_operate_on(WordLoggingWriteSetEntry *e,  unsigned int* addr)
  {
      switch (e->op) {
        case OP_P: *addr += e->val.i;      break;
        case OP_S: *addr -= e->val.i;      break;
        case OP_M: *addr *= e->val.i;      break;
        case OP_D: *addr /= e->val.i;
      };
  }

  template <typename T = long unsigned int>
  void single_operate_on(WordLoggingWriteSetEntry *e,  long unsigned int* addr)
  {
      switch (e->op) {
        case OP_P: *addr += e->val.l;      break;
        case OP_S: *addr -= e->val.l;      break;
        case OP_M: *addr *= e->val.l;      break;
        case OP_D: *addr /= e->val.l;
      };
  }

  template <typename T = float>
  void single_operate_on(WordLoggingWriteSetEntry *e,  float* addr)
  {
      switch (e->op) {
        case OP_P: *addr += e->val.f;      break;
        case OP_S: *addr -= e->val.f;      break;
        case OP_M: *addr *= e->val.f;      break;
        case OP_D: *addr /= e->val.f;
      };
  }

  template <typename T = double>
  void single_operate_on(WordLoggingWriteSetEntry *e,  double* addr)
  {
      switch (e->op) {
        case OP_P: *addr += e->val.d;      break;
        case OP_S: *addr -= e->val.d;      break;
        case OP_M: *addr *= e->val.d;      break;
        case OP_D: *addr /= e->val.d;
      };
  }

  template <typename T = long double>
  void single_operate_on(WordLoggingWriteSetEntry *e,  long double* addr)
  {
      switch (e->op) {
        case OP_P: *addr += e->val.ld;      break;
        case OP_S: *addr -= e->val.ld;      break;
        case OP_M: *addr *= e->val.ld;      break;
        case OP_D: *addr /= e->val.ld;
      };
  }

  template <typename T = __complex__ float>
  void single_operate_on(WordLoggingWriteSetEntry *e,  __complex__ float* addr)
  {
      switch (e->op) {
        case OP_P: *addr += e->val.cf;      break;
        case OP_S: *addr -= e->val.cf;      break;
        case OP_M: *addr *= e->val.cf;      break;
        case OP_D: *addr /= e->val.cf;
      };
  }

  template <typename T = __complex__ double>
  void single_operate_on(WordLoggingWriteSetEntry *e,  __complex__ double* addr)
  {
      switch (e->op) {
        case OP_P: *addr += e->val.cd;      break;
        case OP_S: *addr -= e->val.cd;      break;
        case OP_M: *addr *= e->val.cd;      break;
        case OP_D: *addr /= e->val.cd;
      };
  }

  template <typename T = __complex__ long double>
  void single_operate_on(WordLoggingWriteSetEntry *e,  __complex__ long double* addr)
  {
      switch (e->op) {
        case OP_P: *addr += e->val.cld;      break;
        case OP_S: *addr -= e->val.cld;      break;
        case OP_M: *addr *= e->val.cld;      break;
        case OP_D: *addr /= e->val.cld;
      };
  }

  template <typename T = unsigned long long>
  void single_operate_on(WordLoggingWriteSetEntry *e,  unsigned long long* addr)
  {
      switch (e->op) {
        case OP_P: *addr += e->val.ull;      break;
        case OP_S: *addr -= e->val.ull;      break;
        case OP_M: *addr *= e->val.ull;      break;
        case OP_D: *addr /= e->val.ull;
      };
  }

  /**
   * The log entry for byte logging is complicated by
   *
   *   1) the fact that we store a bitmask
   *   2) that we need to treat the address/value/mask instance variables as
   *      both word types, and byte types.
   *
   *  We do this with unions, which makes the use of these easier since it
   *  reduces the huge number of casts we perform otherwise.
   *
   *  Union naming is important, since the outside world only directly deals
   *  with the word-sized fields.
   */
  struct ByteLoggingWriteSetEntry
  {
      union {
          //void**   addr;
          addr_t addr;
          uint8_t* byte_addr;
      };

      union {
          //void*   val;
          val_t val;
          uint8_t byte_val[sizeof(void*)];
      };

      union {
          uintptr_t mask;
          uint8_t   byte_mask[sizeof(void*)];
      };

      int which;
      int op;


      ByteLoggingWriteSetEntry()
      {
          addr.vp = NULL;
          val.vp = 0;
          mask = (uintptr_t)~0x0;
          which = -1; op = 0;
      }

      /**
       *  Called when we are WAW an address, and we want to coalesce the
       *  write. Trivial for the word-based writeset, but complicated for the
       *  byte-based version.
       *
       *  The new value is the bytes from the incoming log injected into the
       *  existing value, we mask out the bytes we want from the incoming word,
       *  mask the existing word, and union them.
       */
      void update(const ByteLoggingWriteSetEntry& rhs)
      {
          // fastpath for full replacement
          if (__builtin_expect(rhs.mask == (uintptr_t)~0x0, true)) {
              val = rhs.val;
              mask = rhs.mask;
              return;
          }

          // bit twiddling for awkward intersection, avoids looping
          uintptr_t new_val = (uintptr_t)rhs.val.vp;
          new_val &= rhs.mask;
          new_val |= (uintptr_t)val.vp & ~rhs.mask;
          val.vp = (void*)new_val;

          // the new mask is the union of the old mask and the new mask
          mask |= rhs.mask;
      }

      /**
       *  Check to see if the entry is completely contained within the given
       *  address range. We have some preconditions here w.r.t. alignment and
       *  size of the range. It has to be at least word aligned and word
       *  sized. This is currently only used with stack addresses, so we don't
       *  include asserts because we don't want to pay for them in the common
       *  case writeback loop.
       *
       *  The byte-logging writeset can actually accommodate awkward
       *  intersections here using the mask, but we're not going to worry about
       *  that given the expected size/alignment of the range.
       */
      bool filter(void* lower, void* upper)
      {
          return !((long long)addr.vp + 1 < (long long)lower ||
                   (long long)addr.vp >= (long long)upper);
      }


      size_t size()
      {
          switch (which)
          {
            case  0: return sizeof(unsigned char);
            case  1: return sizeof(short unsigned int);
            case  2: return sizeof(unsigned int);
            case  3: return sizeof(long unsigned int);
            case  4: return sizeof(float);
            case  5: return sizeof(double);
            case  6: return sizeof(long double);
            case  7: return sizeof(__complex__ float);
            case  8: return sizeof(__complex__ double);
            case  9: return sizeof(__complex__ long double);
            case 10: return sizeof(unsigned long long);
          }
          return 0;
      }

      /**
       *  If we're byte-logging, we'll write out each byte individually when
       *  we're not writing a whole word. This turns all subword writes into
       *  byte writes, so we lose the original atomicity of (say) half-word
       *  writes in the original source. This isn't a correctness problem
       *  because of our transactional synchronization, but could be a
       *  performance problem if the system depends on sub-word writes for
       *  performance.
       */
      /*void writeback() const
      {
          if (__builtin_expect(mask == (uintptr_t)~0x0, true)) {
              *addr = val;
              return;
          }

          // mask could be empty if we filtered out all of the bytes
          if (mask == 0x0)
              return;

          // write each byte if its mask is set
          for (unsigned i = 0; i < sizeof(val); ++i)
              if (byte_mask[i] == 0xff)
                  byte_addr[i] = byte_val[i];
      }
      */

      void writeback()
      {
          if (__builtin_expect (mask == (uintptr_t)~0x0, true)) {
              switch (which) {
                case 0: *addr.c = val.c;      break;
                case 1: *addr.s = val.s;      break;
                case 2: *addr.i = val.i;      break;
                case 3: *addr.l = val.l;      break;
                case 4: *addr.f = val.f;      break;
                case 5: *addr.d = val.d;      break;
                case 6: *addr.ld = val.ld;    break;
                case 7: *addr.cf = val.cf;    break;
                case 8: *addr.cd = val.cd;    break;
                case 9: *addr.cld = val.cld;  break;
                case 10: *addr.ull = val.ull; break;
                default:
                  assert(0 && "Invalid writeback type");
              };
          }
          // mask could be empty if we filtered out all of the bytes
          if (mask == 0x0)
              return;
          // write each byte if its mask is set
          for (unsigned i = 0; i < size(); ++i)
              if (byte_mask[i] == 0xff)
                  byte_addr[i] = byte_val[i];
      }

      /*** do certain operation to the address, for now just write *addr+val back to *addr ***/
      void operate() const
      {
          switch (which) {
            case 0: *addr.c += val.c;      break;
            case 1: *addr.s += val.s;      break;
            case 2: *addr.i += val.i;      break;
            case 3: *addr.l += val.l;      break;
            case 4: *addr.f += val.f;      break;
            case 5: *addr.d += val.d;      break;
            case 6: *addr.ld += val.ld;    break;
            case 7: *addr.cf += val.cf;    break;
            case 8: *addr.cd += val.cd;    break;
            case 9: *addr.cld += val.cld;  break;
            case 10: *addr.ull += val.ull; break;
            default:
              assert(0 && "Invalid writeback type");
          };
      }

      // [wer] used for each entry in NOrec value list to validate
      bool checkvalue()
      {
          switch (which) {
            case 0: return (*addr.c == val.c);
            case 1: return (*addr.s == val.s);
            case 2: return (*addr.i == val.i);
            case 3: return (*addr.l == val.l);
            case 4: return (*addr.f == val.f);
            case 5: return (*addr.d == val.d);
            case 6: return (*addr.ld == val.ld);
            case 7: return (*addr.cf == val.cf);
            case 8: return (*addr.cd == val.cd);
            case 9: return (*addr.cld == val.cld);
            case 10: return (*addr.ull == val.ull);
            default:
              assert(0 && "Invalid checkvalue type");
          };
      }

      /**
       *  Called during the rollback loop in order to write out buffered writes
       *  to an exception object (represented by the address range). We don't
       *  assume anything about the alignment or size of the exception object.
       */
      void rollback(void** lower, void** upper)
      {
          assert(false && "[wer] NOT implemented yet!.");
          /*
          // two simple cases first, no intersection or complete intersection.
          if (addr + 1 < lower || addr >= upper)
              return;

          if (addr >= lower && addr + 1 <= upper) {
              writeback();
              return;
          }

          // odd intersection
          for (unsigned i = 0; i < sizeof(void*); ++i) {
              if ((byte_mask[i] == 0xff) &&
                  (byte_addr + i >= (uint8_t*)lower ||
                   byte_addr + i < (uint8_t*)upper))
                  byte_addr[i] = byte_val[i];
          }
          */
      }
  };

  template <typename T>
  T wslog_extract_data(ByteLoggingWriteSetEntry e, const T* addr);
  template <typename T = unsigned char>
  unsigned char wslog_extract_data(ByteLoggingWriteSetEntry e, const unsigned char* addr)
  {
      if (((e.mask) & (~0x0)) == (~0x0))
          return e.val.c;
      else {
          unsigned char v = e.val.c;
          v &= ~(e.mask);
          v |= (e.val.c) & e.mask;
          return v;
      }
  }
  template <typename T = short unsigned int>
  short unsigned int wslog_extract_data(ByteLoggingWriteSetEntry e, const short unsigned int* addr)
  {
      if (((e.mask) & (~0x0)) == (~0x0))
          return e.val.s;
      else {
          short unsigned int v = e.val.s;
          v &= ~(e.mask);
          v |= (e.val.s) & e.mask;
          return v;
      }
  }
  template <typename T = unsigned int>
  unsigned int wslog_extract_data(ByteLoggingWriteSetEntry e, const unsigned int* addr)
  {
      if (((e.mask) & (~0x0)) == (~0x0))
          return e.val.i;
      else {
          unsigned int v = e.val.i;
          v &= ~(e.mask);
          v |= (e.val.i) & e.mask;
          return v;
      }
  }
  template <typename T = long unsigned int>
  long unsigned int wslog_extract_data(ByteLoggingWriteSetEntry e, const long unsigned int* addr)
  {
      if (((e.mask) & (~0x0)) == (~0x0))
          return e.val.l;
      else {
          long unsigned int v = e.val.l;
          v &= ~(e.mask);
          v |= (e.val.l) & e.mask;
          return v;
      }
  }
  template <typename T = float>
  float wslog_extract_data(ByteLoggingWriteSetEntry e, const float* addr)
  {
      if (((e.mask) & (~0x0)) == (~0x0))
          return e.val.f;
      else {
          assert(0 && "Invalid extract...");
          return -1;
      }
  }
  template <typename T = double>
  double wslog_extract_data(ByteLoggingWriteSetEntry e, const double* addr)
  {
      if (((e.mask) & (~0x0)) == (~0x0))
          return e.val.d;
      else {
          assert(0 && "Invalid extract...");
          return -1;
      }
  }
  template <typename T = long double>
  long double wslog_extract_data(ByteLoggingWriteSetEntry e, const long double* addr)
  {
      if (((e.mask) & (~0x0)) == (~0x0))
          return e.val.ld;
      else {
          assert(0 && "Invalid extract...");
          return -1;
      }
  }
  template <typename T = __complex__ float>
  __complex__ float wslog_extract_data(ByteLoggingWriteSetEntry e, const __complex__ float* addr)
  {
      if (((e.mask) & (~0x0)) == (~0x0))
          return e.val.cf;
      else {
          assert(0 && "Invalid extract...");
          return -1;
      }
  }
  template <typename T = __complex__ double>
  __complex__ double wslog_extract_data(ByteLoggingWriteSetEntry e, const __complex__ double* addr)
  {
      if (((e.mask) & (~0x0)) == (~0x0))
          return e.val.cd;
      else {
          assert(0 && "Invalid extract...");
          return -1;
      }
  }
  template <typename T = __complex__ long double>
  __complex__ long double wslog_extract_data(ByteLoggingWriteSetEntry e, const __complex__ long double* addr)
  {
      if (((e.mask) & (~0x0)) == (~0x0))
          return e.val.cld;
      else {
          assert(0 && "Invalid extract...");
          return -1;
      }
  }
  template <typename T = unsigned long long>
  unsigned long long wslog_extract_data(ByteLoggingWriteSetEntry e, const unsigned long long* addr)
  {
      if (((e.mask) & (~0x0)) == (~0x0))
          return e.val.ull;
      else {
          unsigned long long v = e.val.ull;
          v &= ~(e.mask);
          v |= (e.val.ull) & e.mask;
          return v;
      }
  }


  template <typename T>
  void wslog_insert_data(ByteLoggingWriteSetEntry* e, T* addr, T val);

  template <typename T = unsigned char>
  void wslog_insert_data(ByteLoggingWriteSetEntry* e, unsigned char* addr, unsigned char val)
  { e->addr.c = addr;      e->val.c = val;     e->which = 0; }

  template <typename T = short unsigned int>
  void wslog_insert_data(ByteLoggingWriteSetEntry* e, short unsigned int* addr, short unsigned int val)
  { e->addr.s = addr;      e->val.s = val;     e->which = 1; }

  template <typename T = unsigned int>
  void wslog_insert_data(ByteLoggingWriteSetEntry* e, unsigned int* addr, unsigned int val)
  { e->addr.i = addr;      e->val.i = val;     e->which = 2; }

  template <typename T = long unsigned int>
  void wslog_insert_data(ByteLoggingWriteSetEntry* e, long unsigned int* addr, long unsigned int val)
  { e->addr.l = addr;      e->val.l = val;     e->which = 3; }

  template <typename T = float>
  void wslog_insert_data(ByteLoggingWriteSetEntry* e, float* addr, float val)
  { e->addr.f = addr;      e->val.f = val;     e->which = 4; }

  template <typename T = double>
  void wslog_insert_data(ByteLoggingWriteSetEntry* e, double* addr, double val)
  { e->addr.d = addr;      e->val.d = val;     e->which = 5; }

  template <typename T = long double>
  void wslog_insert_data(ByteLoggingWriteSetEntry* e, long double* addr, long double val)
  { e->addr.ld = addr;     e->val.ld = val;    e->which = 6; }

  template <typename T = __complex__ float>
  void wslog_insert_data(ByteLoggingWriteSetEntry* e, __complex__ float* addr, __complex__ float val)
  { e->addr.cf = addr;     e->val.cf = val;    e->which = 7; }

  template <typename T = __complex__ double>
  void wslog_insert_data(ByteLoggingWriteSetEntry* e, __complex__ double* addr, __complex__ double val)
  { e->addr.cd = addr;     e->val.cd = val;    e->which = 8; }

  template <typename T = __complex__ long double>
  void wslog_insert_data(ByteLoggingWriteSetEntry* e, __complex__ long double* addr, __complex__ long double val)
  { e->addr.cld = addr;    e->val.cld = val;   e->which = 9; }

  template <typename T = unsigned long long>
  void wslog_insert_data(ByteLoggingWriteSetEntry* e, unsigned long long* addr, unsigned long long val)
  { e->addr.ull = addr;    e->val.ull = val;   e->which = 10; }



#define STM_WS_WORDLOG
  //#define STM_WS_BYTELOG
  /**
   *  Pick a write-set implementation, based on the configuration.
   */
#if defined(STM_WS_WORDLOG)
  typedef WordLoggingWriteSetEntry WriteSetEntry;
#   define STM_WRITE_SET_ENTRY(addr, val, mask) addr, val
#elif defined(STM_WS_BYTELOG)
  typedef ByteLoggingWriteSetEntry WriteSetEntry;
#   define STM_WRITE_SET_ENTRY(addr, val, mask) addr, val, mask
#else
#   error WriteSet logging granularity configuration error.
#endif

} // namespace GTM

#endif // WRITESETENTRY_HPP__
