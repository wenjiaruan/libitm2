/**
 *  Copyright (C) 2011
 *  University of Rochester Department of Computer Science
 *    and
 *  Lehigh University Department of Computer Science and Engineering
 *
 * License: Modified BSD
 *          Please see the file LICENSE.RSTM for licensing information
 */

/**
 *  This file is a combination of the RSTM MiniVector.hpp, WriteSet.hpp, and
 *  WriteSetEntry.hpp files.  Its purpose is to provide an extremely simple
 *  write set interface.
 *
 *  I've cut out a lot of important stuff.  Specifically, this does not use
 *  an O(1) hash, but instead is a simple vector.  It also does not have
 *  support for byte logging, for doing writeback to an exception object for
 *  commit-on-throw behavior, or support for byte masks.  In essence, this
 *  should be thought of as a proof-of-concept for codes that only consist of
 *  word-sized writes.  Note that the RSTM benchmarks can be built in 32-bit
 *  mode to support this goal.
 */

#ifndef RSTM_WRITESET_LITE_HPP
#define RSTM_WRITESET_LITE_HPP 1

#include "common.h"

namespace GTM HIDDEN {

#if 0
  // original WS version... this worked with an older trunk, but does not
  // work any more...

  /**
   * Simple tuple type for address/value pairs
   */
  struct wsentry_t
  {
      char* addr;
      char data[16];
      size_t bytes;

      /*** construct by setting the two values ***/
      wsentry_t(char* _a, char* _dp, size_t _b)
          : addr(_a), bytes(_b)
      {
          for (size_t b = 0; b < bytes; ++b)
              data[b] = *(_dp+b);
      }

      // NB: we don't handle WaW for now, instead dumping to the log twice...

      /*** do a writeback.  This should incorporate a mask eventually ***/
      void writeback() const
      {
          for (size_t b = 0; b < bytes; ++b)
              *(addr+b) = data[b];
      }
  };

  /**
   *  A very lightweight implementation of a write set
   */
  class wslite_t
  {
      unsigned long m_cap;        // capacity
      unsigned long m_size;       // number of used elements
      wsentry_t*    m_elements;   // actual elements in the vector

      /**
       * double the size of the write set
       *
       * NB: Should we have chunks, instead of having an ever-larger array,
       *     since we are going to end up asking for too much memory, and
       *     doing too much copying, otherwise?
       */
      void expand()
      {
          wsentry_t* temp = m_elements;
          m_cap *= 2;
          m_elements =
              static_cast<wsentry_t*>(malloc(sizeof(wsentry_t) * m_cap));
          assert(m_elements);
          memcpy(m_elements, temp, sizeof(wsentry_t)*m_size);
          free(temp);
      }

    public:

      /*** Construct a minivector with a default size */
      wslite_t(const unsigned long capacity = 32)
          : m_cap(capacity), m_size(0),
            m_elements(static_cast<wsentry_t*>(malloc(sizeof(wsentry_t)*m_cap)))
      {
          assert(m_elements);
      }

      ~wslite_t() { free(m_elements); }

      /*** Reset the vector without destroying the elements it holds */
      void reset() { m_size = 0; }

      /*** Insert an element into the minivector */
      void insert(const wsentry_t& data)
      {
          // NB: There is a tradeoff here.  If we put the element into the list
          // first, we are going to have to copy one more object any time we
          // double the list.  However, by doing things in this order we avoid
          // constructing /data/ on the stack if (1) it has a simple
          // constructor and (2) /data/ isn't that big relative to the number
          // of available registers.

          // Push data onto the end of the array and increment the size
          m_elements[m_size++] = data;

          // If the list is full, double the list size, allocate a new array
          // of elements, bitcopy the old array into the new array, and free
          // the old array. No destructors are called.
          if (likely(m_size != m_cap))
              return;
          expand();
      }

      /*** Simple getter to determine the array size */
      unsigned long size() const { return m_size; }

      /*** iterator interface, just use a basic pointer */
      typedef wsentry_t* iterator;

      /*** iterator to the start of the array */
      iterator begin() const { return m_elements; }

      /*** iterator to the end of the array */
      iterator end() const { return m_elements + m_size; }

      /**
       *  Run a writeback by iterating through the log.  Note that we need to
       *  traverse forward, since we may have multiple entries
       */
      inline void writeback()
      {
          for (iterator i = begin(), e = end(); i != e; ++i)
              i->writeback();
      }

      /**
       *  Search function... we need to search in reverse, since we may have
       *  multiple entries.
       *
       *  The log is an in/out parameter, and the bool tells if the search
       *  succeeded.  We need it this way, since we'll ultimately have to
       *  deal with returning partial masks too.
       */
      bool find(wsentry_t& log) const
      {
          // gotta go in reverse
          for (iterator i = end() - 1, e = begin(); i >= e; --i) {
              // [mfs] For now, we require a perfect match...
              if (i->addr == log.addr) {
                  for (size_t b = 0; b < i->bytes; ++b)
                      log.data[b] = i->data[b];
                  return true;
              }
          }
          return false;
      }
  }; // class wslite_t

#else
  /**
   * Simple tuple type for address/value pairs
   */
  struct wsentry_t
  {
      union
      {
          unsigned char*           c;
          short unsigned int*      s;
          unsigned int*            i;
          long unsigned int*       l;
          float*                   f;
          double*                  d;
          long double*             ld;
          __complex__ float*       cf;
          __complex__ double*      cd;
          __complex__ long double* cld;
          unsigned long long*      ull;
          void*                    vp;         // this one is a convenience...
      } addr;

      union
      {
          unsigned char           c;
          short unsigned int      s;
          unsigned int            i;
          long unsigned int       l;
          float                   f;
          double                  d;
          long double             ld;
          __complex__ float       cf;
          __complex__ double      cd;
          __complex__ long double cld;
          unsigned long long      ull;
      } data;

      int which;

      int op; // operation type, "-1" for -=, "1" for +=

      size_t size()
      {
          switch (which)
          {
            case  0: return sizeof(unsigned char);
            case  1: return sizeof(short unsigned int);
            case  2: return sizeof(unsigned int);
            case  3: return sizeof(long unsigned int);
            case  4: return sizeof(float);
            case  5: return sizeof(double);
            case  6: return sizeof(long double);
            case  7: return sizeof(__complex__ float);
            case  8: return sizeof(__complex__ double);
            case  9: return sizeof(__complex__ long double);
            case 10: return sizeof(unsigned long long);
          }
          return 0;
      }


      wsentry_t() { addr.c = NULL; data.c = 0; which = 0; }

      // NB: we don't handle WaW for now, instead dumping to the log twice...

      /*** do a writeback.  This should incorporate a mask eventually ***/
      void writeback() const
      {
          switch (which) {
            case 0: *addr.c = data.c;      break;
            case 1: *addr.s = data.s;      break;
            case 2: *addr.i = data.i;      break;
            case 3: *addr.l = data.l;      break;
            case 4: *addr.f = data.f;      break;
            case 5: *addr.d = data.d;      break;
            case 6: *addr.ld = data.ld;    break;
            case 7: *addr.cf = data.cf;    break;
            case 8: *addr.cd = data.cd;    break;
            case 9: *addr.cld = data.cld;  break;
            case 10: *addr.ull = data.ull; break;
            default:
              assert(0 && "Invalid writeback type");
          };
      }

      /*** do certain operation to the address, for now just write *addr+data back to *addr ***/
      void operate() const
      {
          switch (which) {
            case 0: *addr.c += data.c;      break;
            case 1: *addr.s += data.s;      break;
            case 2: *addr.i += data.i;      break;
            case 3: *addr.l += data.l;      break;
            case 4: *addr.f += data.f;      break;
            case 5: *addr.d += data.d;      break;
            case 6: *addr.ld += data.ld;    break;
            case 7: *addr.cf += data.cf;    break;
            case 8: *addr.cd += data.cd;    break;
            case 9: *addr.cld += data.cld;  break;
            case 10: *addr.ull += data.ull; break;
            default:
              assert(0 && "Invalid writeback type");
          };
      }

      // [wer] used for each entry in NOrec value list to validate
      bool checkvalue()
      {
          switch (which) {
            case 0: return (*addr.c == data.c);
            case 1: return (*addr.s == data.s);
            case 2: return (*addr.i == data.i);
            case 3: return (*addr.l == data.l);
            case 4: return (*addr.f == data.f);
            case 5: return (*addr.d == data.d);
            case 6: return (*addr.ld == data.ld);
            case 7: return (*addr.cf == data.cf);
            case 8: return (*addr.cd == data.cd);
            case 9: return (*addr.cld == data.cld);
            case 10: return (*addr.ull == data.ull);
            default:
              assert(0 && "Invalid checkvalue type");
          };
      }
  };

  /**
   *  A very lightweight implementation of a write set
   */
  class wslite_t
  {
      unsigned long m_cap;        // capacity
      unsigned long m_size;       // number of used elements
      wsentry_t*    m_elements;   // actual elements in the vector

      /**
       * double the size of the write set
       *
       * NB: Should we have chunks, instead of having an ever-larger array,
       *     since we are going to end up asking for too much memory, and
       *     doing too much copying, otherwise?
       */
      void expand()
      {
          wsentry_t* temp = m_elements;
          m_cap *= 2;
          m_elements =
              static_cast<wsentry_t*>(malloc(sizeof(wsentry_t) * m_cap));
          assert(m_elements);
          memcpy(m_elements, temp, sizeof(wsentry_t)*m_size);
          free(temp);
      }

    public:

      /*** Construct a minivector with a default size */
      wslite_t(const unsigned long capacity = 32)
          : m_cap(capacity), m_size(0),
            m_elements(static_cast<wsentry_t*>(malloc(sizeof(wsentry_t)*m_cap)))
      {
          assert(m_elements);
      }

      ~wslite_t() { free(m_elements); }

      /*** Reset the vector without destroying the elements it holds */
      void reset() { m_size = 0; }

      /*** Insert an element into the minivector */
      void insert(const wsentry_t& data)
      {
          // NB: There is a tradeoff here.  If we put the element into the list
          // first, we are going to have to copy one more object any time we
          // double the list.  However, by doing things in this order we avoid
          // constructing /data/ on the stack if (1) it has a simple
          // constructor and (2) /data/ isn't that big relative to the number
          // of available registers.

          // Push data onto the end of the array and increment the size
          m_elements[m_size++] = data;

          // If the list is full, double the list size, allocate a new array
          // of elements, bitcopy the old array into the new array, and free
          // the old array. No destructors are called.
          if (likely(m_size != m_cap))
              return;
          expand();
      }

      /*** Simple getter to determine the array size */
      unsigned long size() const { return m_size; }

      /*** iterator interface, just use a basic pointer */
      typedef wsentry_t* iterator;

      /*** iterator to the start of the array */
      iterator begin() const { return m_elements; }

      /*** iterator to the end of the array */
      iterator end() const { return m_elements + m_size; }

      /**
       *  Run a writeback by iterating through the log.  Note that we need to
       *  traverse forward, since we may have multiple entries
       */
      inline void writeback()
      {
          for (iterator i = begin(), e = end(); i != e; ++i)
              i->writeback();
      }

      /**
       *  Search function... we need to search in reverse, since we may have
       *  multiple entries.
       *
       *  The log is an in/out parameter, and the bool tells if the search
       *  succeeded.  We need it this way, since we'll ultimately have to
       *  deal with returning partial masks too.
       */
      wsentry_t* find(const void* addr) const
      {
          if (m_size == 0)
              return NULL;
          // gotta go in reverse
          for (iterator i = end() - 1, e = begin(); i >= e; --i) {
              // [mfs] For now, we require a perfect address match...
              if (i->addr.vp == addr) {
                  return i;
              }
          }
          return NULL;
      }

      // [wer] for now we use write set to save addr and value as a read value set
      // for NOrec like algorithm, this function is used to validate each entry
      bool valuecheck()
      {
          bool valid = true;
          for (iterator i = begin(), e = end(); i != e; ++i)
              valid &= (i->checkvalue());
          return valid;
      }

      // for ops()
      inline void operate()
      {
          for (iterator i = begin(), e = end(); i != e; ++i)
              i->operate();
      }
  }; // class wslite_t

  //////////////////////////////////////////////////
  // our wsentry_t object is a rather simple struct of unions... the problem
  // is that we need to allow templated read/write functions to access the
  // correct fields of the unions.  To help us out in that effort, we provide
  // two templated functions: the first is for reading the "right" data out
  // of the union, the second is for writing data into the union the "right"
  // way.

  // These functions are for reading from the union
  //
  // NB: the second parameter is a dummy parameter, but we need it because
  // C++ doesn't disambiguate templated functions based solely on return
  // value
  template <typename T>
  T wslog_extract_data(wsentry_t* e, const T* addr);

  template <typename T = unsigned char>
  unsigned char wslog_extract_data(wsentry_t* e, const unsigned char* addr)
  { return e->data.c; }

  template <typename T = short unsigned int>
  short unsigned int wslog_extract_data(wsentry_t* e, const short unsigned int* addr)
  { return e->data.s; }

  template <typename T = unsigned int>
  unsigned int wslog_extract_data(wsentry_t* e, const unsigned int* addr)
  { return e->data.i; }

  template <typename T = long unsigned int>
  long unsigned int wslog_extract_data(wsentry_t* e, const long unsigned int* addr)
  { return e->data.l; }

  template <typename T = float>
  float wslog_extract_data(wsentry_t* e, const float* addr)
  { return e->data.f; }

  template <typename T = double>
  double wslog_extract_data(wsentry_t* e, const double* addr)
  { return e->data.d; }

  template <typename T = long double>
  long double wslog_extract_data(wsentry_t* e, const long double* addr)
  { return e->data.ld; }

  template <typename T = __complex__ float>
  __complex__ float wslog_extract_data(wsentry_t* e, const __complex__ float* addr)
  { return e->data.cf; }

  template <typename T = __complex__ double>
  __complex__ double wslog_extract_data(wsentry_t* e, const __complex__ double* addr)
  { return e->data.cd; }

  template <typename T = __complex__ long double>
  __complex__ long double wslog_extract_data(wsentry_t* e, const __complex__ long double* addr)
  { return e->data.cld; }

  template <typename T = unsigned long long>
  unsigned long long wslog_extract_data(wsentry_t* e, const unsigned long long* addr)
  { return e->data.ull; }



  template <typename T>
  void wslog_insert_data(wsentry_t* e, T* addr, T data);

  template <typename T = unsigned char>
  void wslog_insert_data(wsentry_t* e, unsigned char* addr, unsigned char data)
  { e->addr.c = addr;      e->data.c = data;     e->which = 0; }

  template <typename T = short unsigned int>
  void wslog_insert_data(wsentry_t* e, short unsigned int* addr, short unsigned int data)
  { e->addr.s = addr;      e->data.s = data;     e->which = 1; }

  template <typename T = unsigned int>
  void wslog_insert_data(wsentry_t* e, unsigned int* addr, unsigned int data)
  { e->addr.i = addr;      e->data.i = data;     e->which = 2; }

  template <typename T = long unsigned int>
  void wslog_insert_data(wsentry_t* e, long unsigned int* addr, long unsigned int data)
  { e->addr.l = addr;      e->data.l = data;     e->which = 3; }

  template <typename T = float>
  void wslog_insert_data(wsentry_t* e, float* addr, float data)
  { e->addr.f = addr;      e->data.f = data;     e->which = 4; }

  template <typename T = double>
  void wslog_insert_data(wsentry_t* e, double* addr, double data)
  { e->addr.d = addr;      e->data.d = data;     e->which = 5; }

  template <typename T = long double>
  void wslog_insert_data(wsentry_t* e, long double* addr, long double data)
  { e->addr.ld = addr;     e->data.ld = data;    e->which = 6; }

  template <typename T = __complex__ float>
  void wslog_insert_data(wsentry_t* e, __complex__ float* addr, __complex__ float data)
  { e->addr.cf = addr;     e->data.cf = data;    e->which = 7; }

  template <typename T = __complex__ double>
  void wslog_insert_data(wsentry_t* e, __complex__ double* addr, __complex__ double data)
  { e->addr.cd = addr;     e->data.cd = data;    e->which = 8; }

  template <typename T = __complex__ long double>
  void wslog_insert_data(wsentry_t* e, __complex__ long double* addr, __complex__ long double data)
  { e->addr.cld = addr;    e->data.cld = data;   e->which = 9; }

  template <typename T = unsigned long long>
  void wslog_insert_data(wsentry_t* e, unsigned long long* addr, unsigned long long data)
  { e->addr.ull = addr;    e->data.ull = data;   e->which = 10; }

#endif

} // namespace GTM

#endif // RSTM_WRITESET_LITE_HPP
